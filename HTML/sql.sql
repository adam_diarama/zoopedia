create database zoopediat;
	use zoopediat;

create table admin (
	id_admin int auto_increment primary key,
	nama varchar(30) not null,
	username varchar(30) not null,
	password varchar(20) not null
);

insert into admin values
	(null, 'Adam Diarama', '1400531', '1400531');

create table usul (
	id_usul int auto_increment primary key,
	usulan varchar(50) not null,
	tgl_usul date not null
);

create table hewan (
	id_hewan int auto_increment primary key,
	nama_hewan varchar(50) not null,
	kerajaan varchar(25),
	filum varchar(25),
	kelas varchar(25),
	ordo varchar(25),
	famili varchar(25),
	genus varchar(25),
	spesies varchar(25),
	deskripsi text not null,
	gambar text not null 
);

--buat yang gambar masih belum tahu tipe datanya apa