<!DOCTYPE html>
<html>
	<head>
		<link rel="stylesheet" type="text/css" href="bootstrap/css/bootstrap.min.css">
	</head>

	<body>
		<div class="container">
			<form class="form-horizontal" action="proses_login.php" method="POST">
				<fieldset>
					<legend>Login Admin</legend>
					<div class="form-group">
						<div class="col-lg-12">
							<input name="username" type="text" class="form-control" id="username" placeholder="Username" required>
						</div>
					</div>
					<div class="form-group">
						<div class="col-lg-12">
							<input name="password" type="password" class="form-control" id="password" placeholder="Password" required>
						</div>
					</div>
					<div class="form-group">
						<div class="col-lg-12">
							<button type="submit" class="btn btn-primary">Submit</button>
						</div>
					</div>
					<div class="form-group">
						<div class="col-lg-12">
							<?php
								if(isset($_GET['status'])) {
							?>
								<div class="alert alert-danger" role="alert">Username atau Password Salah</div>
							<?php
								}
							?>
						</div>
					</div>
				</fieldset>
			</form>
		</div>
	</body>
</html>