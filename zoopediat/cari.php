<?php
	include "koneksi.php";
?>

<!DOCTYPE html>
<html>
	<head>
		<link rel="stylesheet" type="text/css" href="bootstrap/css/bootstrap.min.css">
	</head>

	<body>
		<form action="tampil_hewan.php" method="POST">
			<fieldset>
				<div class="form-group">
					<div class="col-lg-12">
						<input type="text" name="search" id="search" class="form-control" placeholder="Cari" required>
					</div>
				</div>
				<div class="form-group">
					<div class="col-lg-12">
						<button type="submit" class="btn btn-primary">Cari</button>
					</div>
				</div>
			</fieldset>
		</body>
	</form>
</html>