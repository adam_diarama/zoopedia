create database zoopedia;
	use zoopedia;

create table admin (
	id_admin int auto_increment primary key,
	nama varchar(30) not null,
	username varchar(30) not null,
	password varchar(20) not null
);

insert into admin values
	(null, 'Adam Diarama', '1400531', '1400531');

create table usul (
	id_usul int auto_increment primary key,
	usulan varchar(50) not null,
	status int not null
);

create table hewan (
	id_hewan int auto_increment primary key,
	nama_hewan varchar(50) not null,
	latin varchar(50) not null,
	kerajaan varchar(25),
	filum varchar(25),
	kelas varchar(25),
	ordo varchar(25),
	famili varchar(25),
	genus varchar(25),
	spesies varchar(25),
	deskripsi text not null 
);

create table ghewan (
	id_gambar int auto_increment primary key,
	id_hewan int not null,
	gambar1 varchar(255) not null,
	gambar longblob,
	foreign key (id_hewan) references hewan(id_hewan)
);

insert into hewan values(null, 'Kucing', 'Felis Catus', 'Animalia', 'Chordata', 'Mammalia', 'Karnivora', 'Felidae', 'Felis', 'F.catus', 'Test aja');
insert into ghewan values(null, 1, 'Hewan_g/kucing.jpg', LOAD_FILE('Hewan_g\kucing.jpg'));
insert into ghewan values(null, 1, 'Hewan_g/kucing1.jpg', LOAD_FILE('Hewan_g\kucing1.jpg'));
insert into hewan values(null, 'Anjing', 'Canis Familiaris', 'Animalia', 'Chordata', 'Mammalia', 'Karnivora', 'Canidae', 'Canis', 'C.lupus', 'Test aja yang ke 2');
insert into ghewan values(null, 1, 'http://cdn0-a.production.liputan6.static6.com/medias/1186590/big/011208100_1459313205-20160330-Ilustrasi-Kucing-iStockphoto10.jpg', LOAD_FILE('http://cdn0-a.production.liputan6.static6.com/medias/1186590/big/011208100_1459313205-20160330-Ilustrasi-Kucing-iStockphoto10.jpg'));

insert into usul values(null, '', 0);

--buat yang gambar masih belum tahu tipe datanya apa