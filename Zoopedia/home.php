<!DOCTYPE html>
<html>
	<head>
		<title>Zoopedia</title>
		<link rel="stylesheet" type="text/css" href="bootstrap/css/bootstrap.min.css">
		<link rel="stylesheet" type="text/css" href="css/home.css">
		<script src="https://ajax.googleapis.com/ajax/libs/jquery/1.12.2/jquery.min.js"></script>
		<script src="bootstrap/js/bootstrap.min.js"></script>
	</head>

	<body>
		<div class="container">
			<div class="row" style="margin-top:30px;">
				<div class="col-md-10 col-md-offset-1 mainpage" style="background-color:white; height:auto;">
					<div class="row">
						<div class="col-md-5 logos">
							<img src="Image/logo.png" alt="logo">
						</div>
						<div class="col-md-5 namas" style="display: table-cell;">
							<div class="namas1">
								<p class="namas11"><strong>Situs Pencarian Data Hewan Terbesar</strong></p>
								<p class="namas12"><strong>Zoopedia</strong></p>
								<form action="proses_cari.php" method="POST">						
									<input type="text" name="nama" id="nama" class="input1" placeholder="Temukan hewan yang kamu cari" required>
									<button type="submit" class="searchicon"><img src="Image/search-icon.png"></button>

									<br/><input type="hidden" name="kingdom" id="kingdom" value="">
									<!-- <span class="glyphicon glyphicon-search" style="margin-left:-20px;" onclick="mySearch()">
									</span> -->
								</form>
								<p class="spesifik1"><a href="spesifik.php">Klik disini untuk pencarian lebih spesifik</a></p>
							</div>
						</div>
						<div class="col-md-2 usuls	">
							<!-- <a href="#"><div class="kotakusul">Usulkan Hewan</div>
							<img src="Image/tambah.png" class="tambah"></a> -->
							<button type="button" data-toggle="modal" data-target="#popUsul" class="kotakusul">Usulkan Hewan</button>
							<img src="Image/tambah.png" class="tambah"></a>

							<!-- Modal -->
								<div id="popUsul" class="modal fade" role="dialog">
								  <div class="modal-dialog">

								    <!-- Modal content-->
								    <div class="modal-content">
								      <form method="POST" onsubmit="return submitusul();">
									      <div class="modal-header">
										     
										     <button type="button" class="close" data-dismiss="modal">&times;</button>
										     <h4>Usulkan Nama Hewan</h4>
									      </div>
									      <div class="modal-body">
									        <input type="text" name="namausul" id="namausul" placeholder="Usulkan nama hewan" class="form-control" required>
									      	<br/>
									      	<input type="submit" value="Usul" class="btn btn-default">
									      </div>
									  </form>
								    </div>

								  </div>
								</div>
							<!-- Akhir Modal -->
						</div>
					</div>
				</div>
			</div>

			<div class="row">
				<div class="col-md-10 col-md-offset-1 footer">
					<div class="row">
						<div class="col-md-6 footlog">
							<img src="Image/logo2.png" style="margin-top:-5px;">
							<div class="foottext"><strong>Zoopedia.</strong></div>
						</div>
						<div class="col-md-6 cpyright" align="right">
						<strong>
							admin@zoopedia.com <img src="Image/mail.png">
							<br/>
							Copyright &copy 2016. Reserved to Hisyam H Fuadi
						</strong>
						</div>
					</div>
				</div>
			</div>
		</div>

		<script>
			function submitusul() {
				var namausul = $('#namausul').val();
				$.ajax({
					type: 'POST',
					url: 'tambah_usul.php',
					data:
					{
						usulan:namausul
					},
					success: function(response) {
						$('#success_para').html("Data Berhasil Dimasukkan");
					}
					
				});

				$('#popUsul').modal("hide");

				alert("Hewan Diusulkan");
				return false;
			}
		</script>
	</body>
</html>