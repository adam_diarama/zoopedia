<!DOCTYPE html>
<html>
	<head>
		<title>Zoopedia</title>
		<link rel="stylesheet" type="text/css" href="bootstrap/css/bootstrap.min.css">
		<link rel="stylesheet" type="text/css" href="css/home.css">
	</head>

	<body>
		<div class="container">
			<div class="row" style="margin-top:30px;">
				<div class="col-md-10 col-md-offset-1 mainpage" style="background-color:white; height:auto;">
					<div class="row">

						<!-- Sisi Kanan -->
						<div class="col-md-5 logos">
							<img src="Image/logo.png" alt="logo">
						</div>
						<!-- Akhir Sisi Kanan -->

						<!-- Bagian Tengah -->
						<div class="col-md-5 namas" style="display: table-cell;">
							<div class="namas1">
								<h2 style="font-size:24px;"><strong>Pencarian Spesifik</strong></h2>

								<form action="proses_cari.php" method="POST">
									<input type="text" id="nama" name="nama" placeholder="Nama Hewan" style="width:220px;">

									<table style="margin-top:10px;">
										<tr>
											<td><input type="text" name="kerajaan" id="kerajaan" placeholder="kingdom" value="" style="width:105px;"></td>
											<td>
												<input type="text" name="filum" id="filum" placeholder="filum" value="" style="width:105px; margin-left:10px;">
											</td>
										</tr>

										<tr>
											<td>
												<input type="text" name="kelas" id="kelas" placeholder="kelas" value="" style="width:105px; margin-top:10px;">
											</td>
											<td>
												<input type="text" name="ordo" id="ordo" placeholder="ordo" value="" style="width:105px; margin-left:10px; margin-top:10px;">
											</td>
										</tr>

										<tr>
											<td>
												<input type="text" name="famili" id="famili" placeholder="famili" value="" style="width:105px; margin-top:10px;">
											</td>
											<td>
												<input type="text" name="genus" id="genus" placeholder="genus" value="" style="width:105px; margin-left:10px; margin-top:10px;">
											</td>
										</tr>

										<tr>
											<td colspan="2" align="center">
												<input type="text" name="spesies" id="spesies" placeholder="Spesies" value=""
												style="width:105px; margin-top:10px;">
											</td>
										</tr>

										<tr>
											<td colspan="2">
												<input type="submit" value="Submit" style="margin-top:20px;">
											</td>
										</tr>
									</table>
								</form>
							</div>
						</div>
						<!-- Akhir Bagian Tengah -->

						<div class="col-md-2 usuls	">
							<a href="#"><div class="kotakusul">Usulkan Hewan</div>
							<img src="Image/tambah.png" class="tambah"></a>
						</div>
					</div>
				</div>
			</div>

			<div class="row">
				<div class="col-md-10 col-md-offset-1 footer">
					<div class="row">
						<div class="col-md-6 footlog">
							<img src="Image/logo2.png" style="margin-top:-5px;">
							<div class="foottext"><strong>Zoopedia.</strong></div>
						</div>
						<div class="col-md-6 cpyright" align="right">
						<strong>
							admin@zoopedia.com <img src="Image/mail.png">
							<br/>
							Copyright &copy 2016. Reserved to Hisyam H Fuadi
						</strong>
						</div>
					</div>
				</div>
			</div>
		</div>

		<script>
		// 	function mySearch() {
		// 		window.location.href = 'halaman_searching.html'
		// 	}
		// </script>
	</body>
</html>