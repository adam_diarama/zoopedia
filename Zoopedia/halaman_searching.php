<?php
	include "koneksi.php";
	session_start();

	$nama = $_SESSION['nama'];
	$kingdom = $_SESSION['kingdom'];
	$filum = $_SESSION['filum'];
	$kelas = $_SESSION['kelas'];
	$ordo = $_SESSION['ordo'];
	$famili = $_SESSION['famili'];
	$genus = $_SESSION['genus'];
	$spesies = $_SESSION['spesies'];

	//$kingdom = $_POST['kingdom'];
	
	// Query
	$query = "select * from hewan where ";

	$jumlah = 0;

	if($nama != "")
	{
		$query .= "nama_hewan like '%$nama%' ";
		$jumlah++;
	}

	if($kingdom != "")
	{
		if($jumlah > 0)
		{
			$query .= "and ";
		}
		$jumlah++;

		$query .= "kerajaan like '%$kingdom%' ";
	}

	if($filum   != "")
	{
		if($jumlah > 0)
		{
			$query .= "and ";
		}
		$jumlah++;
		$query .= "filum like '%$filum%' ";
	}

	if($kelas != "")
	{
		if($jumlah > 0)
		{
			$query .= "and ";
		}
		$jumlah++;

		$query .= "kelas like '%$kelas%' ";
	}

	if($ordo != "")
	{
		if($jumlah > 0)
		{
			$query .= "and ";
		}
		$jumlah++;

		$query .= "ordo like '%$ordo%' ";
	}

	if($famili != "")
	{
		if($jumlah > 0)
		{
			$query .= "and ";
		}
		$jumlah++;

		$query .= "famili like '%$famili%' ";
	}

	if($genus != "")
	{
		if($jumlah > 0)
		{
			$query .= "and ";
		}
		$jumlah++;

		$query .= "genus like '%$genus%' ";
	}

	if($spesies != "")
	{
		if($jumlah > 0)
		{
			$query .= "and ";
		}
		$jumlah++;

		$query .= "spesies like '%$spesies%'";
	}

	$query .= ";";
	// Akhir Query


	$hasil = mysql_query($query);

	$aktifid = $_GET['aktifid'];
	$j = 0;
?>

<!DOCTYPE html>
<html>
	<head>
		<title>Zoopedia</title>
		<link rel="stylesheet" type="text/css" href="bootstrap/css/bootstrap.min.css">
		<link rel="stylesheet" type="text/css" href="css/halaman_searching.css">
		<script src="https://ajax.googleapis.com/ajax/libs/jquery/1.12.2/jquery.min.js"></script>
		<script src="bootstrap/js/bootstrap.min.js"></script>
	</head>

	<body>
		<div class="container">
			<div class="row" style="margin-top:30px;">
				<div class="col-md-10 col-md-offset-1 mainpage" style="height:500px; background-color:white;">
					<div class="row">
						<div class="col-md-3" style="margin-top:10px;">
						<img src="Image/logo3.png" style="margin-top:-5px;">
							<div class="logo"><strong>Zoopedia.</strong>
							</div>
						</div>
						<div class="col-md-6" style="margin-top:10px;">
							<form action="proses_cari.php" method="POST">						
									<input type="text" name="nama" id="nama" class="input1" placeholder="Temukan hewan yang kamu cari" required>
									<button type="submit" class="searchicon"><img src="Image/search-icon.png" style="height:25px; width=auto; "></button>

									<br/><input type="hidden" name="kingdom" id="kingdom" value="">
									<!-- <span class="glyphicon glyphicon-search" style="margin-left:-20px;" onclick="mySearch()">
									</span> -->
							</form>
						</div>
						<div class="col-md-3 spesifik" align="right">
							<a href="spesifik.php">Klik disini
							<br/>
							Untuk pencarian lebih spesifik</a>
						</div>
					</div>

					<div class="row" style="margin-top:50px;">
						<div class="col-md-3">
							<div class="hasilnama"><strong>Hasil Pencarian !</strong></div>
							<div class="hasilnama1" style="padding-top:20px; padding-left:20px;">
							<?php

								while($hewan = mysql_fetch_array($hasil))
								{
									
							?>
								<?php
									if($aktifid == $hewan['id_hewan'])
									{
								?>
								<p class="aktif"><strong><a><?php echo $hewan['nama_hewan']; ?></a></strong></p>
								<?php
									} else {
										$id = $hewan['id_hewan'];
								?>
								<p><strong><a href="halaman_searching.php?aktifid= <?php echo $id; ?>"><?php echo $hewan['nama_hewan']; ?></a></strong></p>
								<?php
									}
								?>
							<?php
									$j++;
								}
							?>
							</div>
							<div class="usulkans">
								Hewan tidak ditemukan?
								<p data-toggle="modal" data-target="#popUsul">Klik disini untuk usulkan</p>
								
								<!-- Modal -->
								<div id="popUsul" class="modal fade" role="dialog">
								  <div class="modal-dialog">

								    <!-- Modal content-->
								    <div class="modal-content">
								      <form method="POST" onsubmit="return submitusul();">
									      <div class="modal-header">
										     <button type="button" class="close" data-dismiss="modal">&times;</button>
										     Usulkan Nama Hewan
									      </div>
									      <div class="modal-body">
									        <input type="text" name="namausul" id="namausul" placeholder="Usulkan nama hewan" class="form-control" required>
									      	<br/>
									      	<input type="submit" value="Usul" class="btn btn-default">
									      </div>
									  </form>
								    </div>

								  </div>
								</div>
							<!-- Akhir Modal -->

							</div>
						</div>

						<!-- PHP untuk mengambil gambar dari database -->
						<?php
							$queryg = "select gambar1 from ghewan where id_hewan = '$aktifid'";
							$hasilg = mysql_query($queryg);
							$j = 0;
							while($gambars = mysql_fetch_array($hasilg))
							{
								$j++;
							}

						?>

						<!-- SlideShow Gambar -->
						<div class="col-md-6">
							<div id="myCarousel" class="carousel slide" data-ride="carousel">
							  
							  <!-- Indicators -->
							  <ol class="carousel-indicators">
							  	<?php
							  		for($i = 0; $i < $j; $i++)
							  		{
							  			if($i == 0)
							  			{

							  	?>
							    <li data-target="#myCarousel" data-slide-to="<?php echo $i; ?>" class="active"></li>
							    <?php
							    		} else {
							    ?>
							    <li data-target="#myCarousel" data-slide-to="<?php echo $i; ?>"></li>
							  	<?php
							  			}
							  		}
							  	?>
							  </ol>

							  <!-- Wrapper for slides -->
							  <div class="carousel-inner" role="listbox">
							    
							  <?php
							  	$k = 0;
							  	$queryg = "select gambar1 from ghewan where id_hewan = '$aktifid'";
								$hasilg = mysql_query($queryg);
							  	while($gambars = mysql_fetch_array($hasilg))
							  	{
							  		if($k == 0)
							  		{
							  ?>
							    <div class="item active">
							      <img src="<?php echo $gambars['gambar1']; ?>" style="height:300px;">
							    </div>
							   <?php
							   		} else {
							   ?>

							    <div class="item">
							      <img src="<?php echo $gambars['gambar1']; ?>" style="height:300px;">
							    </div>
							   <?php
							   		}
							   		$k++;
							   	}
							   ?>
							  </div>

							  <!-- Left and right controls -->
							  <a class="left carousel-control" href="#myCarousel" role="button" data-slide="prev">
							    <span class="glyphicon glyphicon-chevron-left" aria-hidden="true"></span>
							    <span class="sr-only">Previous</span>
							  </a>
							  <a class="right carousel-control" href="#myCarousel" role="button" data-slide="next">
							    <span class="glyphicon glyphicon-chevron-right" aria-hidden="true"></span>
							    <span class="sr-only">Next</span>
							  </a>
							</div>	
						</div>

						<!-- Bagian Kanan Deskripsi -->
						<div class="col-md-3 deskripsi">
							<h1 style="margin-top:-10px;">
								<?php
									$query = "select * from hewan where id_hewan = '$aktifid'";
									$hasil = mysql_query($query);

									while($hewan = mysql_fetch_array($hasil))
									{
										echo $hewan['nama_hewan'];
								?>
								<hr style="height:1px; margin-bottom: -20px;; margin-top:0; visibility:hidden;">
								<i>(<?php echo $hewan['latin']; ?>)</i>
							</h1>

							<table>
								<tr>
									<td style="width:70px;">Kingdom</td>
									<td style="width:10px;">:</td>
									<td><?php echo $hewan['kerajaan']; ?></td>
								</tr>
								<tr>
									<td>Filum</td>
									<td>:</td>
									<td><?php echo $hewan['filum']; ?></td>
								</tr>
								<tr>
									<td>Kelas</td>
									<td>:</td>
									<td><?php echo $hewan['kelas']; ?></td>
								</tr>
								<tr>
									<td>Ordo</td>
									<td>:</td>
									<td><?php echo $hewan['ordo']; ?></td>
								</tr>
								<tr>
									<td>Famili</td>
									<td>:</td>
									<td><?php echo $hewan['famili']; ?></td>
								</tr>
								<tr>
									<td>Genus</td>
									<td>:</td>
									<td><?php echo $hewan['genus']; ?></td>
								</tr>
								<tr>
									<td>Spesies</td>
									<td>:</td>
									<td><?php echo $hewan['spesies']; ?></td>
								</tr>
							</table>
							<br/>
							<p><?php echo $hewan['deskripsi']; ?></p>
								<?php
									}
								?>
						</div>
					</div>
				</div>
			</div>

			<div class="row">
				<div class="col-md-10 col-md-offset-1 footer">
					<div class="row">
						<div class="col-md-6 footlog">
							<img src="Image/logo2.png" style="margin-top:-5px;">
							<div class="foottext"><strong>Zoopedia.</strong></div>
						</div>
						<div class="col-md-6 cpyright" align="right">
						<strong>
							admin@zoopedia.com <img src="Image/mail.png">
							<br/>
							Copyright &copy 2016. Reserved to Hisyam H Fuadi
						</strong>
						</div>
					</div>
				</div>
			</div>
		</div>

		<script>
			function submitusul() {
				var namausul = $('#namausul').val();
				$.ajax({
					type: 'POST',
					url: 'tambah_usul.php',
					data:
					{
						usulan:namausul
					},
					success: function(response) {
						$('#success_para').html("Data Berhasil Dimasukkan");
					}
					
				});

				$('#popUsul').modal("hide");

				alert("Hewan Diusulkan");
				return false;
			}
		</script>
	</body>
</html>