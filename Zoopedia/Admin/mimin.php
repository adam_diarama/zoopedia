<!DOCTYPE html>
<html>
	<head>
		<title>Zoopedia</title>
		<link rel="stylesheet" type="text/css" href="../bootstrap/css/bootstrap.min.css">
		<link rel="stylesheet" type="text/css" href="../css/mimin.css">
		<script src="https://ajax.googleapis.com/ajax/libs/jquery/1.12.2/jquery.min.js"></script>
		<script src="../bootstrap/js/bootstrap.min.js"></script>
	</head>

	<body>
		<div class="container">
			<div class="row" style="margin-top:30px;">
				<div class="col-md-10 col-md-offset-1 mainpage" style="background-color:white; height:500px; margin-top:10px;">
					<div align="center" style="margin-top:100px;">
						<div class="kotakadmin">
							<div class="row">
								<div class="col-md-12 headeradmin">
									<img src="../Image/logo4.png" style="margin-top:-10px;">
									<div class="headertext"><strong>Zoopediaadmin.</strong></div>
								</div>
							</div>
							<div class="row">
								<div class="col-md-12 inputadmin">
									<form action="proses_login.php" method="POST">
										<input type="text" class="form-control" name="username" id="username" placeholder="Username..." required style="width:250px;">

										<input type="password" class="form-control" name="password" id="password" placeholder="Password..." required style="width:250px; margin-top:30px;">

										<div align="right" style="margin-top:30px; margin-right:30px;">
											<button type="submit" class="btn btn-default tombollogin">Login</button>
										</div>
									</form>
								</div>
							</div>
							<?php
									if(isset($_GET['status']))
									{

								?>
									<div class="alert alert-danger fade in">
										<a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
										Login Gagal!
									</div>
								<?php
									}
								?>
						</div>
					</div>
				</div>
			</div>

			<div class="row">
				<div class="col-md-10 col-md-offset-1 footer">
					<div class="row">
						<div class="col-md-6 footlog">
							<img src="../Image/logo2.png" style="margin-top:-5px;">
							<div class="foottext"><strong>Zoopedia.</strong></div>
						</div>
						<div class="col-md-6 cpyright" align="right">
						<strong>
							admin@zoopedia.com <img src="../Image/mail.png">
							<br/>
							Copyright &copy 2016. Reserved to Hisyam H Fuadi
						</strong>
						</div>
					</div>
				</div>
			</div>
		</div>

		<script>
			function submitusul() {
				var namausul = $('#namausul').val();
				$.ajax({
					type: 'POST',
					url: 'tambah_usul.php',
					data:
					{
						usulan:namausul
					},
					success: function(response) {
						$('#success_para').html("Data Berhasil Dimasukkan");
					}
					
				});

				$('#popUsul').modal("hide");

				alert("Hewan Diusulkan");
				return false;
			}
		</script>
	</body>
</html>