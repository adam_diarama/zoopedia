<?php
	include "../koneksi.php";
	session_start();

	if(!isset($_SESSION['username']))
	{
		header('location:mimin.php');
	}

	$username = $_SESSION['username'];

	if(!isset($_GET['page']))
	{
		$page = 0;
	} else {
		$page = $_GET['page'];

		if($page == 203)
		{
			$nama = $_POST['nama'];
			$namalatin = $_POST['namalatin'];
			$kerajaan = $_POST['kerajaan'];
			$filum = $_POST['filum'];
			$kelas = $_POST['kelas'];
			$ordo = $_POST['ordo'];
			$famili = $_POST['famili'];
			$genus = $_POST['genus'];
			$spesies = $_POST['spesies'];
		}
	}
?>

<!DOCTYPE html>
<html>
	<head>
		<title>Zoopedia</title>
		<link rel="stylesheet" type="text/css" href="../bootstrap/css/bootstrap.min.css">
		<link rel="stylesheet" type="text/css" href="../css/mimin-home.css">
		<script src="https://ajax.googleapis.com/ajax/libs/jquery/1.12.2/jquery.min.js"></script>
		<script src="../bootstrap/js/bootstrap.min.js"></script>
	</head>

	<body>
		<div class="container">
			<div class="row" style="margin-top:30px;">
				<div class="col-md-10 col-md-offset-1 mainpage" style="background-color:white; height:500px; margin-top:10px;">
					<div class="row">
						<div class="col-md-11 headermimin">
							<img src="../Image/logo4.png" style="margin-top:-15px;">
							<div class="headertext"><strong>Zoopediaadmin.</strong></div>
						</div>
						<div class="col-md-1 headerlogout" align="right">
							<div style="margin-top:20px;"><a href="logout.php" style="text-decoration:none; color:white;">Logout</a></div>
						</div>
					</div>

					<div class="row">
						<div class="col-md-3">
							<div class="sidebar">
								<p style="padding-left:10px; padding-top:10px;"><strong>Hello <?php echo $username; ?></strong></p>

								<p style="padding-left:10px; padding-top:20px;"><strong><a href="mimin-home.php?page=1" style="text-decoration:none; color:#c0c0c0;">Data Usulan</a></strong></p>
								<p style="padding-left:10px; padding-top:5px;"><strong><a href="mimin-home.php?page=202" style="text-decoration:none; color:#c0c0c0;">Tambah Data</a></strong></p>
							</div>
						</div>
						<div class="col-md-8">
							<div class="kotakmain">
								<?php
									if($page == 0)
									{
								?>
								<!-- Halaman Home Admin -->
								<br/>
								<h1 style="margin-top:10px; margin-left:10px;">Selamat Datang di Zoopedia Admin</h1>
								<?php
									} else if($page > 0 && $page < 202)
									{
										$i = 1;
										$batas = $page * 5;
										$query = "select * from usul where status=0";
										$hasil = mysql_query($query);
								?>
								<!-- Halama Usul Admin -->
								<br/>
								<div align="center">
								<table border="1" align="center">
									<tr align="center">
										<td style="width:50px;">No</td>
										<td>Usulan</td>
										<td>Aksi</td>
									</tr>

									<?php
										while($usul = mysql_fetch_array($hasil))
										{
											if($i > $batas - 5 && $i <= $batas)
											{
									?>
									<tr align="center">
										<td align="center"><?php echo $i; ?></td>
										<td style="width:200px;"><?php echo $usul['usulan']; ?></td>
										<td style="width:100px; height:30px;" align="center"><button type="button" class="btn btn-primary btn-xs"><a href="selesai.php?id=<?php echo $usul['id_usul'];?>" style="text-decoration:none; color:white;">Selesai</a></button></td>
									</tr>
									<?php
											}
										$i++;
										}

										$jumlah = $i / 5;
										$sisa = $i % 5;
										if($sisa != 1)
										{
											$jumlah++;
										}
									?>
								</table>

								<br/>
								<ul class="pagination">
									<?php
										for($j = 1; $j < $jumlah; $j++)
										{
											if($j == $page)
											{
									?>
									<li class="active"><a href="mimin-home.php?page=<?php echo $j; ?>"><?php echo $j; ?></a></li>
									<?php
											} else {
									?>
									<li><a href="mimin-home.php?page=<?php echo $j; ?>"><?php echo $j; ?></a></li>
									<?php
											}
										}
									?>
								</ul>

								<?php
									} else if($page == 202 || $page == 203)
									{

								?>
									<br/>
									<h2 align="center">Tambah Data Hewan</h2>
									<br/>
									<?php
										if($page == 202)
										{
									?>
									<form action="mimin-home.php?page=203" method="POST">
										
									<table>
										<tr>
											<td><input type="text" name="nama" id="nama" placeholder="Nama Hewan" class="form-control" style="width:150px; height:30px; margin-left:10px;" required></td>

											<td><input type="text" name="namalatin" id="namalatin" placeholder="Nama Latin" class="form-control" style="width:150px; height:30px; margin-left:10px; display:inline;" required></td>
										</tr>

										<tr>
											<td><input type="text" name="kerajaan" id="kerajaan" placeholder="Kingdom" class="form-control" style="width:150px; height:30px; margin-left:10px; margin-top:10px;" required></td>

											<td><input type="text" name="filum" id="filum" placeholder="Filum" class="form-control" style="width:150px; height:30px; margin-left:10px; margin-top:10px;" required></td>
										</tr>

										<tr>
											<td><input type="text" name="kelas" id="kelas" placeholder="Kelas" class="form-control" style="width:150px; height:30px; margin-left:10px; margin-top:10px;" required></td>

											<td><input type="text" name="ordo" id="ordo" placeholder="Ordo" class="form-control" style="width:150px; height:30px; margin-left:10px; margin-top:10px;" required></td>
										</tr>

										<tr>
											<td><input type="text" name="famili" id="famili" placeholder="Famili" class="form-control" style="width:150px; height:30px; margin-left:10px; margin-top:10px;" required></td>

											<td><input type="text" name="genus" id="genus" placeholder="Genus" class="form-control" style="width:150px; height:30px; margin-left:10px; margin-top:10px;" required></td>
										</tr>

										<tr>
											<td><input type="text" name="spesies" id="spesies" placeholder="Spesies" class="form-control" style="width:150px; height:30px; margin-left:10px; margin-top:10px;" required></td>
										</tr>

										<tr>
											<td><input type="submit" class="btn btn-primary" value="Next" style="margin-left:10px; margin-top:10px;"></td>
										</tr>
									</table>

										<!-- <table>
											<tr>
												<td><input type="text" name="nama" id="nama" placeholder="Nama Hewan" class="form-control" style="width:150px; height:30px; margin-left:10px;" required></td>

												<td><input type="text" name="namalatin" id="namalatin" placeholder="Nama Latin" class="form-control" style="width:150px; height:30px; margin-left:10px; display:inline;" required></td>

												<td><input type="text" name="kerajaan" id="kerajaan" placeholder="Kingdom" class="form-control" style="width:150px; height:30px; margin-left:10px;" required></td>

												<td><input type="text" name="filum" id="filum" placeholder="Filum" class="form-control" style="width:150px; height:30px; margin-left:10px;" required></td>
											</tr>

											<tr>
												<td><input type="text" name="kelas" id="kelas" placeholder="Kelas" class="form-control" style="width:150px; height:30px; margin-left:10px; margin-top:10px;" required></td>

												<td><input type="text" name="ordo" id="ordo" placeholder="Ordo" class="form-control" style="width:150px; height:30px; margin-left:10px; margin-top:10px;" required></td>

												<td><input type="text" name="famili" id="famili" placeholder="Famili" class="form-control" style="width:150px; height:30px; margin-left:10px; margin-top:10px;" required></td>

												<td><input type="text" name="genus" id="genus" placeholder="Genus" class="form-control" style="width:150px; height:30px; margin-left:10px; margin-top:10px;" required></td>
											</tr>

											<tr>
												<td><input type="text" name="spesies" id="spesies" placeholder="Spesies" class="form-control" style="width:150px; height:30px; margin-left:10px; margin-top:-55px;" required></td>

												<td colspan="2">
													<div class="form-group">
														<textarea class="form-control" rows="5" id="deskripsi" name="deskripsi" placeholder="Deskripsi" style="margin-left:10px; margin-top:10px; width:310px;" required></textarea>
													</div>
												</td>

												<td>
													<input type="file" name="img" id="img" required>
												</td>
											</tr>
										</table> -->

									</form>
									<?php
										} else if($page == 203)
										{
									?>

										<form action="tambahdata.php" method="POST" enctype="multipart/form-data">
											<input type="hidden" name="nama" id="nama" value="<?php echo $nama; ?>">
											<input type="hidden" name="namalatin" id="namalatin" value="<?php  echo $namalatin; ?>">
											<input type="hidden" name="kerajaan" id="kerajaan" value="<?php echo $kerajaan; ?>">
											<input type="hidden" name="filum" id="filum"
											value="<?php echo $filum; ?>">
											<input type="hidden" name="kelas" id="kelas" value="<?php echo $kelas; ?>">
											<input type="hidden" name="ordo" id="ordo" value="<?php echo $ordo; ?>">
											<input type="hidden" name="famili" id="famili" value="<?php echo $famili; ?>">
											<input type="hidden" name="genus" id="genus" value="<?php echo $genus; ?>">
											<input type="hidden" name="spesies" id="spesies" value="<?php echo $spesies; ?>">
										
											<div class="form-group">
												
												<label for="deksripsi"></label>
												<textarea class="form-control" rows="5" id="deskripsi" name="deskripsi" placeholder="Deskripsi Hewan" style="margin-left:10px; width:400px;" required></textarea>
											</div>

											<input type="file" name="img[]" id="img" style="margin-left:10px; margin-top:10px;" multiple>

											<br/>

											<input type="submit" class="btn btn-primary" value="Submit" style="margin-left:10px; margin-top:20px;">
										</form>

								<?php
										}
									}
								?>
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>

			<div class="row">
				<div class="col-md-10 col-md-offset-1 footer">
					<div class="row">
						<div class="col-md-6 footlog">
							<img src="../Image/logo2.png" style="margin-top:-5px;">
							<div class="foottext"><strong>Zoopedia.</strong></div>
						</div>
						<div class="col-md-6 cpyright" align="right">
						<strong>
							admin@zoopedia.com <img src="../Image/mail.png">
							<br/>
							Copyright &copy 2016. Reserved to Hisyam H Fuadi
						</strong>
						</div>
					</div>
				</div>
			</div>
		</div>
	</body>
</html>